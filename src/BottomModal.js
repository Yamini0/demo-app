import React, { useState, useCallback, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Alert,
} from 'react-native';
import Modal from 'react-native-modal';

const data = {
  productName: 'Bombay Dry Gin',
  productImage: require('./assets/img.png'),
  category: 'Gin',
  price: '1377.31',
};

export const BottomModal = ({ isVisible, onClose }) => {
  const [count, setCount] = useState(1);
  const [totalAmount, setTotalAmount] = useState(data.price);

  useEffect(() => {
    if (data.price) {
      sumTotal(data.price);
    }
  }, [count, sumTotal, data.price]);

  const incrementPrice = useCallback(() => {
    setCount((c) => c + 1);
  }, []);

  const decrementPrice = useCallback(() => {
    setCount((c) => c - 1);
  }, []);

  const sumTotal = useCallback(
    (a, b) => {
      var first = parseFloat(a);
      var second = b ? parseFloat(b) : 0;
      const sum = first + second;
      var finalSum = count > 1 ? sum * count : sum;
      setTotalAmount(finalSum.toFixed(2));
    },
    [count]
  );

  const onFooterPressed = useCallback((item) => {
    Alert.alert(item);
  }, []);

  return (
    <Modal
      animationIn={'slideInUp'}
      isVisible={isVisible}
      style={styles.modalContainer}
      backdropColor='#000'
      backdropOpacity={0.6}
      onBackButtonPress={onClose}
      onBackdropPress={onClose}
    >
      <View style={styles.modalContent}>
        <View style={styles.collar} />

        <View>
          {/* header */}
          <View style={styles.headerContainer}>
            <View style={styles.productImagContainer}>
              <Image source={data.productImage} style={styles.image} />
            </View>
            <View style={styles.productDetailsContainer}>
              <Text style={styles.name}>{data.productName}</Text>
              <Text style={styles.category}>Category: {data.category}</Text>
              <View style={styles.priceContainer}>
                <Text style={styles.price}>MRP ₹{data.price}</Text>
              </View>
              <View style={styles.quantityContainer}>
                <Text style={styles.quantity}>750 ml</Text>
              </View>
            </View>
          </View>

          {/* container */}
          <View style={styles.container}>
            <TouchableOpacity
              onPress={count <= 1 ? undefined : () => decrementPrice()}
              style={styles.calculationButton}
            >
              <Text>-</Text>
            </TouchableOpacity>
            <View style={styles.countContainer}>
              <Text style={styles.itemCount}>{count}</Text>
              <Text style={styles.totalPrice}>₹{totalAmount}</Text>
            </View>
            <TouchableOpacity
              onPress={incrementPrice}
              style={styles.calculationButton}
            >
              <Text>+</Text>
            </TouchableOpacity>
          </View>

          {/* footer */}

          <View style={styles.footerContainer}>
            <TouchableOpacity
              style={styles.footerButton}
              onPress={() => onFooterPressed('QR Scan')}
            >
              <Image
                source={require('./assets/qr.png')}
                style={styles.footerButtonIcon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.footerButton}
              onPress={() => onFooterPressed('Add Product')}
            >
              <Image
                source={require('./assets/notification.jpeg')}
                style={styles.footerButtonIcon}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  modalContainer: {
    marginHorizontal: 0,
    justifyContent: 'flex-end',
    marginVertical: 0,
  },
  modalContent: {
    backgroundColor: '#fff',
    borderTopEndRadius: 24,
    borderTopStartRadius: 24,
    paddingHorizontal: 24,
    paddingTop: 16,
    paddingBottom: 24,
  },
  collar: {
    alignSelf: 'center',
    backgroundColor: '#D9D9D9',
    width: 40,
    borderRadius: 16,
    height: 4,
    marginBottom: 32,
  },
  headerContainer: {
    flexDirection: 'row',
  },
  productImagContainer: {
    width: 100,
    height: 200,
  },
  productDetailsContainer: {
    marginStart: 16,
    marginTop: 16,
  },
  name: {
    color: '#1d1f1e',
    fontWeight: '600',
    fontSize: 22,
  },
  category: {
    color: '#1d1f1e',
    fontWeight: '500',
    fontSize: 12,
    lineHeight: '24',
  },
  priceContainer: {
    paddingTop: 42,
  },
  price: {
    color: '#1d1f1e',
    fontWeight: '700',
    fontSize: 20,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  quantityContainer: {
    backgroundColor: '#98d4e380',
    borderRadius: 16,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    paddingVertical: 4,
  },
  quantity: {
    color: '#1b53a8',
    fontWeight: '400',
    fontSize: 14,
  },
  container: {
    marginVertical: 16,
    paddingHorizontal: 52,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  countContainer: {
    alignItems: 'center',
  },
  itemCount: {
    color: '#000',
    fontSize: 32,
    fontWeight: '600',
  },
  totalPrice: {
    color: '#686869',
    fontSize: 12,
    fontWeight: '500',
  },
  calculationButton: {
    width: 50,
    height: 32,
    borderWidth: 0.8,
    borderRadius: 8,
    borderColor: '#51525480',
    alignItems: 'center',
    justifyContent: 'center',
  },
  footerContainer: {
    flexDirection: 'row',
    backgroundColor: '#000',
    height: 50,
    alignItems: 'center',
    paddingVertical: 16,
    justifyContent: 'space-between',
    borderRadius: 42,
    paddingHorizontal: 52,
    marginTop: 32,
    marginBottom: 16,
  },
  footerButton: {
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  footerButtonIcon: {
    width: 20,
    height: 20,
  },
});
