import React, { useCallback, useState } from 'react';
import { StyleSheet, Button, Image } from 'react-native';
import SafeAreaView, { SafeAreaProvider } from 'react-native-safe-area-view';
import { BottomModal } from './src/BottomModal';

const App = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const onCloseModal = useCallback(() => {
    setIsModalVisible(false);
  }, []);

  const onOpenModal = useCallback(() => {
    setIsModalVisible(true);
  }, []);

  return (
    <SafeAreaProvider>
      <SafeAreaView style={styles.wrapper}>
        <Button title='Tap to open' onPress={onOpenModal} />
        {/* <Image
          source={require('./assets/icon.png')}
          // source={{
          //   uri: 'https://pbs.twimg.com/profile_images/486929358120964097/gNLINY67_400x400.png',
          // }}
          style={{ width: 200, height: 200, backgroundColor: '#a0a' }}
        /> */}
      </SafeAreaView>
      <BottomModal isVisible={isModalVisible} onClose={onCloseModal} />
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;
